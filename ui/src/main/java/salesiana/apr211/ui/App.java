package salesiana.apr211.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import salesiana.apr211.ui.common.WindowComponent;
import salesiana.apr211.ui.pages.Dashboard;

public class App extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    WindowComponent dashboard = new Dashboard(primaryStage);
    dashboard.openWindow();
  }
}
