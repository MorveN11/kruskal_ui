package salesiana.apr211.ui.node;

import javafx.scene.Cursor;
import javafx.scene.control.Button;

public class AppNode extends Button {

  public AppNode(String text) {
    setText(text);
    setCursor(Cursor.HAND);
    setStyle("-fx-font-size: 15;");
    getStyleClass().add("circle");
  }
}
