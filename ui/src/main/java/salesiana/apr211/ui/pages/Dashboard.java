package salesiana.apr211.ui.pages;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import salesiana.apr211.ui.common.ComposableComponent;
import salesiana.apr211.ui.common.WindowComponent;
import salesiana.apr211.ui.node.AppNode;

public class Dashboard implements ComposableComponent, WindowComponent {

  private final GridPane gridPane;
  private final Stage window;

  public Dashboard(Stage window) {
    this.gridPane = new GridPane();
    this.window = window;
  }

  @Override
  public void configureComponent() {
    gridPane.setVgap(66.5);
    gridPane.setAlignment(Pos.CENTER);
    gridPane.setPadding(new Insets(10));
    gridPane.setStyle("-fx-background-color: #141B28;");
    GridPane.setHgrow(gridPane, Priority.ALWAYS);
  }

  @Override
  public Node getComponent() {
    return gridPane;
  }

  @Override
  public void compose() {
    HBox box = new HBox();
    Node appNode = new AppNode("node1");
    box.getChildren().add(appNode);
    box.setAlignment(Pos.CENTER);
    gridPane.add(box, 0, 0);
  }

  @Override
  public Stage getWindow() {
    return window;
  }

  @Override
  public Node getWindowComponent() {
    return this.build();
  }

  @Override
  public int getHeight() {
    return 900;
  }

  @Override
  public int getWidth() {
    return 1440;
  }
}
