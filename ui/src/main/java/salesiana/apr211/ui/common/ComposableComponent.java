package salesiana.apr211.ui.common;

import javafx.scene.Node;

public interface ComposableComponent extends Component {

  void compose();

  @Override
  default Node build() {
    compose();
    return Component.super.build();
  }
}
