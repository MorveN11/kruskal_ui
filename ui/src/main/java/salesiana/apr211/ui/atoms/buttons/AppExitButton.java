//package salesiana.apr211.ui.atoms.buttons;
//
//import javafx.stage.Stage;
//import salesiana.apr211.ui.atoms.image.AppButtonImage;
//
//public class AppExitButton implements IAppExit {
//
//  private final AppButtonImage button;
//  private final Stage stage;
//
//  public AppExitButton(Stage stage) {
//    this.stage = stage;
//    button = new AppButtonImage("exit_icon.png");
//  }
//
//  public Stage getStage() {
//    return stage;
//  }
//
//  @Override
//  public AppButtonImage getButton() {
//    return button;
//  }
//
//  @Override
//  public void buttonAction() {
//    stage.close();
//  }
//}
